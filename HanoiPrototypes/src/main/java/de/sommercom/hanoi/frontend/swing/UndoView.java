package de.sommercom.hanoi.frontend.swing;

import de.sommercom.hanoi.game.Command;
import de.sommercom.hanoi.game.Hanoi;
import de.sommercom.hanoi.game.MoveCommand;

import java.util.Observable;
import java.util.Observer;

import javax.swing.JTextArea;

public class UndoView extends JTextArea implements Observer {
	// TODO: Überall alles final, immutable klassen soweit möglich
	private static final long serialVersionUID = -5784404008960036330L;

	private final Hanoi hanoi;

	public UndoView(Hanoi hanoi) {
		this.hanoi = hanoi;
		
		setLineWrap(false);
		setEditable(false);
		
		hanoi.addObserver(this);
	}

	@Override
	public void update(Observable o, Object arg) {
		String terror = "";
		for (Command cmd : hanoi.getUndoList()) {
			if (cmd instanceof MoveCommand) {
				terror += "("+((MoveCommand)cmd).getFrom() + ", " + ((MoveCommand)cmd).getTo() + "),\n";
			}
		}
		this.setText("Moves: "+hanoi.getMoveCount()+"\n\n"+"Undos: "+hanoi.getUndoCount()+"\n\n" + terror);
	}
}
