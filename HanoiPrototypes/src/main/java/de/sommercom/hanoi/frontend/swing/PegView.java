package de.sommercom.hanoi.frontend.swing;

import de.sommercom.hanoi.game.Peg;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.Observable;
import java.util.Observer;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.border.EtchedBorder;

public class PegView extends JLabel implements Observer {
	//TODO: logik fertig machen, undocount, movecount, print to string (android/kindle)
	//TODO: Observer für Änderungen
	//TODO: onclick, entweder markierte scheib auflegen, oder scheibe markieren (im viewcontroller)
	//TODO: move (a,b) in model (game)
	//TODO: render: platten angeben
	
	private static final long serialVersionUID = -5040551868911133454L;
	private final Peg peg;
	private final GameFrame frontend;

	private final class PegListener implements MouseListener {
		private final Peg peg;
		private final GameFrame frontend;
		
		public PegListener(Peg peg, GameFrame frontend) {
			super();
			this.peg = peg;
			this.frontend = frontend;
		}
		
		@Override
		public void mouseClicked(MouseEvent e) {
			frontend.onPegClicked(peg);
		}

		@Override
		public void mouseEntered(MouseEvent e) {

		}

		@Override
		public void mouseExited(MouseEvent e) {

		}

		@Override
		public void mousePressed(MouseEvent e) {
		}

		@Override
		public void mouseReleased(MouseEvent e) {
		}
	}

	public PegView(GameFrame frontend, Peg peg) {
		this.frontend = frontend;
		this.peg = peg;
		
		setBorder(BorderFactory.createEtchedBorder(EtchedBorder.RAISED));
		update(null,null);
		addMouseListener(new PegListener(this.peg,this.frontend));
		
		// TODO: DND!
		
		
		peg.addObserver(this);
	}

	@Override
	public void update(Observable arg0, Object arg1) {
		String text = peg+": "+peg.disksToString();
		setText(text);
	}
}
