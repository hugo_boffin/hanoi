package de.sommercom.hanoi.frontend.swing;

import de.sommercom.hanoi.game.Hanoi;
import de.sommercom.hanoi.game.Peg;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.HeadlessException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.SpinnerModel;
import javax.swing.SpinnerNumberModel;

public class GameFrame extends JFrame implements ActionListener {
	//TODO: memory leak beim restart??
	
	private static final long serialVersionUID = 1234812148606356056L;
	
	private JButton reset;
	private JButton undo;
	private JPanel northPanel;
	private JLabel disksLabel;
	private SpinnerModel disksModel;
	private JSpinner disksField;
	private JLabel pegsLabel;
	private SpinnerModel pegsModel;
	private JSpinner pegsField;
	private JPanel centerPanel;

	private Peg selectedPeg = null;
	private Hanoi game;
	
	public GameFrame() throws HeadlessException {
		setTitle("Simple Swing Frontend for Hanoi - Kindle HD Prototype");
		setSize(640, 480);
		setMinimumSize(getSize());
		setLocationRelativeTo(null);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		this.setLayout(new BorderLayout());

		northPanel = new JPanel();

		disksLabel = new JLabel("Disks:");
		disksModel = new SpinnerNumberModel(8, 3, 1000, 1);
		disksField = new JSpinner(disksModel);

		pegsLabel = new JLabel("Pegs:");
		pegsModel = new SpinnerNumberModel(4, 3, 1000, 1);
		pegsField = new JSpinner(pegsModel);

		this.add(northPanel, BorderLayout.NORTH);

		reset = new JButton("(Re)Start");
		undo = new JButton("Undo");

		northPanel.add(disksLabel);
		northPanel.add(disksField);
		northPanel.add(pegsLabel);
		northPanel.add(pegsField);

		northPanel.add(reset);
		northPanel.add(undo);

		centerPanel = new JPanel();

		this.add(centerPanel, BorderLayout.CENTER);

		this.pack();

		reset.addActionListener(this);
		undo.addActionListener(this);
	}

	public void onPegClicked(Peg peg) {
		if (selectedPeg == null) {
			selectedPeg = peg;
			System.out.println(game.getIndexOfPeg(peg)+" selected...");
		} else {
			if (selectedPeg != peg) {
				game.moveDisk(game.getIndexOfPeg(selectedPeg), game.getIndexOfPeg(peg));
			}
			selectedPeg = null;
			System.out.println("...moved to "+game.getIndexOfPeg(peg));
		}
	}

	public static void main(String[] args) {
		GameFrame swingFrontend = new GameFrame();
		swingFrontend.setVisible(true);
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		if (arg0.getSource() == reset) {
			int disks = (Integer) disksField.getValue();
			int pegs = (Integer) pegsField.getValue();
			
			game = new Hanoi(pegs, disks, 0, 1, true);
			
			this.selectedPeg = null;
			
			centerPanel.setLayout(new GridLayout(1, pegs));
			centerPanel.removeAll();
			for (int i = 0; i < pegs; i++) {
				centerPanel.add(new PegView(this, game.getPeg(i)));
			}

			UndoView undoList = new UndoView(game);
			centerPanel.add(undoList);
		
			this.pack();
		}
		if (arg0.getSource() == undo) {
			game.UndoLastCommand();
		}
	}
}
