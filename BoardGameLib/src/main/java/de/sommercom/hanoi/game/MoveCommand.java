package de.sommercom.hanoi.game;

/**
 * Moves one disk from one peg to another
 * @author Christian
 *
 */
public class MoveCommand implements Command {
	private final Hanoi hanoiGame;
	private final int from;
	private final int to;
	
	public int getFrom() {
		return from;
	}

	public int getTo() {
		return to;
	}

	public MoveCommand(Hanoi hanoiGame, int from, int to) {
		super();
		this.hanoiGame = hanoiGame;
		this.from = from;
		this.to = to;
	}
	
	public int getMoveCount()
	{
		if (hanoiGame.isNearby()) {
			return Math.abs(from-to);		
		} else {
			return 1;
		}
	}

	@Override
	public boolean execute() {
		return hanoiGame.move(from, to);
	}

	@Override
	public void undo() {
		hanoiGame.move(to, from);
	}
}
