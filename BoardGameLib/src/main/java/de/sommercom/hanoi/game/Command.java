package de.sommercom.hanoi.game;

public interface Command {
	boolean execute();
	void undo();
	//boolean undoable() ??
}
