package de.sommercom.hanoi.game;

public enum HanoiEvent {
	DISK_MOVED,
	GAME_WON,
	UNDO //Required?
}
