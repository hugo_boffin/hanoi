package de.sommercom.hanoi.game;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Observable;
import java.util.Stack;

public class Hanoi extends Observable {
	// TODO: Commander Pattern
	// TODO: on peg added
	// TODO: on peg removed
	// TODO: on game started?
	// TODO: on game ended?
	// TODO: on gamestate changed?
	// TODO: on disk moved
	// TODO: on undo ??
	// TODO: onMoveCountChanged ...
	// TODO: oder alles in gamestate changed, wegen MoveCount anzeige
	// TODO: System.out.print("("+ (char)(65+from) + " , " + (char)(65+to)+"),");
	// TODO: Memory Leaks stopfen, werden alle alten game states (OBserver!!) wirklich freigegeben?

	private int undos = 0;
	private final Stack<Command> history = new Stack<Command>();	
	private final boolean nearby;
	private final ArrayList<Peg> pegs = new ArrayList<Peg>();
	private final int sourcePeg;
	private final int targetPeg;
	
	public Hanoi(int pegs, int disks,int sourcePeg, int targetPeg, boolean nearby) {
		this.nearby = nearby;
		this.sourcePeg = sourcePeg;
		this.targetPeg = targetPeg;
		
		for (int i = 0; i < pegs; i++) {
			this.pegs.add(new Peg(""+(char)(65+i), i == this.sourcePeg ? disks : 0));
		}
	}
	
	public boolean moveDisk(int from, int to) {
		Command cmd = new MoveCommand(this, from, to);
				
		boolean result = cmd.execute();
		if (result == true)
			history.push(cmd);
				
		return result;
	}
	
	public boolean moveDisk(Peg from, Peg to) {
		boolean result = moveDisk(getIndexOfPeg(from),getIndexOfPeg(to));
		return result;
	}
	
	public int historySize() {
		return history.size();
	}
		
	public void UndoLastCommand() {
		if (!history.empty()) {
			history.pop().undo();
			undos++;
		}
	}
	
	public Peg getPeg(int i) {
		return pegs.get(i);
	}
	
	public int getPegCount() {
		return pegs.size();
	}
	
	public List<Command> getUndoList() {
		return Collections.unmodifiableList(history);
	}
	
	public int getUndoCount() {
		return undos;
	}

	public boolean isNearby() {
		return nearby;
	}
	
	public int getMoveCount() {
		int moves = 0;
		for (Command cmd : history) {
			if (cmd instanceof MoveCommand) {
				moves += ((MoveCommand)cmd).getMoveCount();
			}
		}
		return moves;
	}

	@Override
	public String toString() {
		String temp = "";
		for(Peg t : pegs) {
			temp += t + "  ";
		}
		return temp;
	}

	public int getIndexOfPeg(Peg peg) throws NullPointerException {
		int index = pegs.indexOf(peg);
		if (index == -1) {
			throw new NullPointerException("Peg not in this game!");
		} else {
			return index;
		}
	}

	public int getDiskCount() {
		int diskCount = 0;
		for (Peg peg : pegs) {
			diskCount += peg.getDiskCount();
		}
		return diskCount;
	}

	public List<Peg> getPegs() {
		return Collections.unmodifiableList(pegs);
	}

	public boolean rawMoveDisk(int from, int to) {
		if (isNearby() && Math.abs(from-to) != 1)
			return false;
	
		if (getPeg(from).isEmpty())
			return false;
	
		if (getPeg(from).topDiskSize() > getPeg(to).topDiskSize())
			return false;		
		
		getPeg(to).addTopDisk(getPeg(from).popTopDisk());
		
		// Check Game Situation - End?
		
		if (getPeg(targetPeg).getDiskCount() == getDiskCount() ) {
			// Game won!			
			setChanged();
			notifyObservers(HanoiEvent.GAME_WON);
		}
		
		return true;
	}

	public boolean rawNearbyBatchMove(int from, int to) {
		if (to < from) {
			for (int i = from; i>to; i--)
				if (getPeg(from).topDiskSize() < getPeg(i-1).topDiskSize())
					return false;		
	
			for (int i = from; i>to; i--) {
				if ( !rawMoveDisk(i, i-1))
					return false;
			}
		} else {
			for (int i = from; i<to; i++)
				if (getPeg(from).topDiskSize() < getPeg(i+1).topDiskSize())
					return false;		
	
			for (int i = from; i<to; i++) {
				if ( !rawMoveDisk(i, i+1))
					return false;
			}				
		}
		
		return true;
	}

	boolean move(int from, int to) {
		boolean result;
		if (isNearby() && Math.abs(from-to) != 1) {
			result = rawNearbyBatchMove(from, to);
		} else {
			result = rawMoveDisk(from, to);
		}
		
		setChanged();
		notifyObservers(HanoiEvent.DISK_MOVED);
		
		return result;
	}
}