package de.sommercom.hanoi.game;

import java.util.Collection;
import java.util.Collections;
import java.util.Observable;
import java.util.Stack;

// index und buchstaben draufmappen

public class Peg extends Observable {
	private Stack<Integer> disks = new Stack<Integer>();
	private String rodName;
	
	public Peg(String rodName, int numDisks) {
		this.rodName = rodName;
		
		for (int i = numDisks; i > 0;i--) {
			disks.add(i);
		}
	}
	
	public int getDiskCount() {
		return disks.size();
	}
	
	public int popTopDisk() {
		//TODO: Empty?
		setChanged();
		int topDisk = disks.pop();
	    notifyObservers();
	    return topDisk;
	}
	
	public void addTopDisk(int disk) {
		//TODO: x size check
		setChanged();
		disks.add(disk);
	    notifyObservers();
	}

	@Override
	public String toString() {
		return rodName;
	}
	
	public String disksToString() {
		return disks.toString();
	}
	
	public int topDiskSize() {
		if (disks.empty())
			return Integer.MAX_VALUE;
		return disks.peek();
	}

	public boolean isEmpty() {
		return disks.empty();
	}	
	
	public Collection<Integer> getDisks() {
		return Collections.unmodifiableCollection(disks);
	}
}