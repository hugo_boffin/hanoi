package de.sommercom.hanoi;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

/*
 * interpolierte scheibenfarbe
 * scheibe: umrandet, text in scheibe
 * im spiel activity leiste wegmachen,hauptleiste einstellbar
 * programname, icon
beim landscape switch laedt spiel neu!?

Fensterresolution basierend auf hoehe usw., momentan fest in pegview
Elemente oben
	Zoom
	Undo
	MainMenu
staebe
Collider Activity Switching
Speicherbugs?
Wischen statt Click-Click, evtl. ueber preferences
nette (japanische) Retro musik, schaltbar
actionbar ingame

onTouchDragMove: game.checkMove()? -> anzeige
*/
public class MainMenuActivity extends Activity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_menu);
    }
    
    //TODO: Unschoen, lieber ueber streng getypte Intents...
    public void startNewGame(View view) {
		Intent intent = new Intent(this, GameActivity.class);
		startActivity(intent);
    }

//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        getMenuInflater().inflate(R.menu.activity_main_menu, menu);
//        return true;
//    }
}
