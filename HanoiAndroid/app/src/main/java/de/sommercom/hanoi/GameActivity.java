package de.sommercom.hanoi;

import de.sommercom.hanoi.game.Hanoi;
import android.app.Activity;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;

public class GameActivity extends Activity {
	private Hanoi game;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_game);

		View view = findViewById(R.id.mainLayout);

		int colors[] = { 0xff255779 , 0xff3e7492, 0xffa6c0cd };
		GradientDrawable g = new GradientDrawable(GradientDrawable.Orientation.TOP_BOTTOM, colors);
		view.setBackgroundDrawable(g);
		
		// Model Creation bzw. Laden
		game = new Hanoi(3, 3, 0, 1, false);	
				
		LinearLayout layout = (LinearLayout) findViewById(R.id.mainLayout);
		
		final GameView gameView = new GameView(this, game);
		layout.addView(gameView);
	}

	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
	    MenuInflater inflater = getMenuInflater();
	    inflater.inflate(R.menu.game_menu, menu);
	    return super.onCreateOptionsMenu(menu);
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
	    switch (item.getItemId()) {
	        case R.id.undo:
	        	game.UndoLastCommand();
	        	return true;
	        default:
	            return super.onOptionsItemSelected(item);
	    }
	}
}
