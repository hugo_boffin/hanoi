package de.sommercom.hanoi;

import de.sommercom.hanoi.game.Hanoi;
import de.sommercom.hanoi.game.HanoiEvent;
import de.sommercom.hanoi.game.Peg;

import java.util.Observable;
import java.util.Observer;

import android.annotation.TargetApi;
import android.app.ActionBar;
import android.app.Activity;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.os.Build;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.widget.TextView;
import android.widget.Toast;

public class GameView extends View implements Observer {
	private final Hanoi game;
	private final GradientDrawable gd;
	final int stroke = 1;
	
	public GameView(Context context, final Hanoi game) {
		super(context);
		this.game = game;
		this.game.addObserver(this);
		
		this.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));

		this.setOnTouchListener(new OnTouchListener() {
			private Peg sourcePeg;
			
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				//Activity host = (Activity) GameView.this.getContext();
				
				int pegIndex = Math.max(0, Math.min(game.getPegCount()-1, (int)event.getX() / ( getWidth() / game.getPegCount())));
				//final TextView selView = (TextView) host.findViewById(R.id.selectionTextView);
			
				switch (event.getAction()) {
				case MotionEvent.ACTION_DOWN:
					sourcePeg = game.getPeg(pegIndex);
					//selView.setText(pegIndex + " -> ...");
					break;
				case MotionEvent.ACTION_MOVE:
					//selView.setText(game.getIndexOfPeg(sourcePeg) + " -> " + pegIndex);
					break;
				case MotionEvent.ACTION_CANCEL:
					break;
				case MotionEvent.ACTION_UP:
					Peg targetPeg = game.getPeg(pegIndex);
					game.moveDisk(sourcePeg, targetPeg);
					//selView.setText(game.getIndexOfPeg(sourcePeg) + " -> " + pegIndex + ".");
					break;
				default:
				}
				try {
					Thread.sleep(16);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				return true;
			}
		});

		gd = new GradientDrawable();
		gd.setStroke(stroke, Color.BLACK);	
		
		update(null, null);
	}
	
	public GameView(Context context) {
		super(context);
		this.game = null;
		this.gd = null;
	}
	
	@Override
	protected void onDraw(Canvas canvas) {
		super.onDraw(canvas);

		// Hoehe = Pixel / Scheiben Gesamt * Level
		int height = getHeight();
		int width = getWidth() / game.getPegCount();
		int pegHeightOptimal = (int)((float)height / (float)game.getDiskCount()); // full scale height
		int pegHeightAspect = (int)((float)width * 0.1f); // aspect ratio height
		int pegHeight = Math.min(pegHeightOptimal, pegHeightAspect);
				
		int ox = 0;
		for (Peg peg : game.getPegs()) {
			final int offset = ox*width;
			ox++;
			int level = 0;
			for (Integer i : peg.getDisks()) {
				final float intDisk = 1.0f /  ((float)game.getDiskCount()-1) * ((float)i-1);
				final int pegWidth = (int)((float)width*0.20f + (float)width*0.70f * intDisk);
				final int mixed = mixColors(Color.GREEN,Color.CYAN,intDisk);
				
				gd.setCornerRadius(pegHeight/2);
				gd.setColor(mixed);			
				gd.setBounds(offset + width / 2 - pegWidth / 2, height - (level+1)*pegHeight,
						  offset + width / 2 + pegWidth / 2, height - (level)*pegHeight);
				gd.draw(canvas);

				level++;
			}
		}
	}

	private int mixColors(int a, int b, float intDisk) {
		return Color.rgb(
				(int)(Color.red(a)*(1.0f-intDisk) + Color.red(b)*intDisk), 
				(int)(Color.green(a)*(1.0f-intDisk) + Color.green(b)*intDisk), 
				(int)(Color.blue(a)*(1.0f-intDisk) + Color.blue(b)*intDisk)
				);
	}

	@TargetApi(Build.VERSION_CODES.HONEYCOMB)
	public void update(Observable arg0, Object arg1) {
		// FIXME Welches Event trat im Model auf ???
		
		Activity host = (Activity) this.getContext();

		if (Build.VERSION.SDK_INT > Build.VERSION_CODES.GINGERBREAD_MR1) {
			
			ActionBar actionBar = host.getActionBar();
			actionBar.setSubtitle(game.getMoveCount()+" Moves, "+game.getUndoCount()+" Undos");
		} else {
			// FIXME Simple Labels???
			final TextView moveView = (TextView) host.findViewById(R.id.moveCountTextView);
			final TextView undoView = (TextView) host.findViewById(R.id.undoCountTextView);
			
			moveView.setText(game.getMoveCount()+" Moves");
			undoView.setText(game.getUndoCount()+" Undos");
		}
		
		if(arg1!=null)
			switch ((HanoiEvent)arg1) {
			case DISK_MOVED:
				// TODO: Log
				break;
			case GAME_WON:
				Toast.makeText(this.getContext(), "Congrats! You have won the game!", Toast.LENGTH_LONG).show();
				break;
			case UNDO:
				break;
			default:
				break;
		}
		
		invalidate();
	}
}
